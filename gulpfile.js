var gulp = require("gulp"),
	path = require("path"),
	notify = require("gulp-notify"),
	concat = require("gulp-concat"),
	sass = require("gulp-sass"),
	iconfont = require("gulp-iconfont"),
	iconfontCss = require("gulp-iconfont-css"),
	webserver = require("gulp-webserver");

sass.compiler = require("node-sass");

var handleError = function() {
	var args = Array.prototype.slice.call(arguments);

	notify
		.onError({
			title: "Compile Error!",
			message: "<%= error.message %>"
		})
		.apply(this, args);

	this.emit("end");
};

gulp.task("sass", function() {
	gulp
		.src(["app/dev/sass/*.scss"])
		.pipe(sass().on("error", handleError))
		.pipe(gulp.dest("app/dist/css/"));
});

gulp.task("iconfont", function() {
	gulp
		.src(["app/dev/icon/*.svg"])
		.pipe(
			iconfontCss({
				path: "scss",
				fontName: "icon",
				targetPath: "../../../dev/sass/_icons.scss",
				fontPath: "../font/icon/"
			})
		)
		.pipe(
			iconfont({
				fontName: "icon",
				prependUnicode: true,
				normalize: true
			})
		)
		.pipe(gulp.dest("app/dist/font/icon/"));
});

gulp.task("javascript", function() {
	gulp
		.src([
			"node_modules/jquery/dist/jquery.js",
			"node_modules/inputmask/dist/jquery.inputmask.min.js",
			"node_modules/jquery-validation/dist/jquery.validate.min.js",
			"node_modules/aos/dist/aos.js",
			"app/dev/js/app.js"
		])
		.pipe(concat("app.js").on("error", handleError))
		.pipe(gulp.dest("app/dist/js/"));
});

gulp.task("webserver", function() {
	gulp.src("app/dist").pipe(
		webserver({
			livereload: true,
			directoryListing: true,
			open: true,
			port: 9911
		})
	);
});

gulp.task("watch", function() {
	gulp.watch("app/dev/sass/**/*.scss", ["sass"]);
	gulp.watch("app/dev/js/*.js", ["javascript"]);
});

gulp.task("default", ["iconfont", "sass", "javascript"]);
gulp.task("dev", ["watch", "iconfont", "sass", "javascript", "webserver"]);
