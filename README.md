# paytrek #

### Requirements ###
* [Node.js]
* [Npm]
* [Gulp]


### Requirements Installation ###
```
#!bash

download and install node.js
$ sudo npm install -g npm
$ sudo npm install -g gulp
$ sudo npm install -g bower
```


### Project Installation ###
In project directory:
```
#!bash

$ npm install
$ gulp (production)
$ gulp dev (webserver)
```

[Node.js]: http://nodejs.org
[Npm]: http://npmjs.com
[Gulp]: http://gulpjs.com
