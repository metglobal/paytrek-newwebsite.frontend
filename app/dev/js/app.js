const paytrek = {
  init: function() {
    AOS.init();
    paytrek.header();
    paytrek.mobileToggle();
    paytrek.applyFormMasks();
    paytrek.applyFormMask();

    // faq
    paytrek.faqToggle();
  },

  header: function() {
    $(window).on('scroll', () => {
      const scrollTop = $(window).scrollTop();
      if(scrollTop > 60) {
        $('.js-header').addClass('fixed');
      } else {
        $('.js-header').removeClass('fixed');
      }
    });
  },

  mobileToggle: function() {
    $('.js-mobile-toggle').on('click', function(){
      $('.js-navigation').addClass('active');
      $('.js-menu-backdrop').addClass('active');
    });
    $('.js-menu-backdrop').on('click', function(){
      $('.js-navigation').removeClass('active');
      $('.js-menu-backdrop').removeClass('active');
    });    
  },

  applyFormMasks: function() {
    $('.js-phone-input').inputmask("0599 999 99 99");
  },

  applyFormMask: function() {
    $(".js-apply-form").submit(function(e) {
      e.preventDefault();
    }).validate({
      rules: {
        nameSurname: {
          required: true,
          normalizer: function(value) {
            return $.trim(value);
          }
        },
        phone: {
          required: true,
          normalizer: function(value) {
            return $.trim(value);
          }
        },
        email: {
          required: true,
          normalizer: function(value) {
            return $.trim(value);
          }
        },
        type: {
          required: true,
          normalizer: function(value) {
            return $.trim(value);
          }
        }                        
      },
      messages: {
        nameSurname: {
          required: 'Lütfen adınızı ve soyadınızı girin.',
        },
        phone: {
          required: 'Lütfen telefon numaranızı girin.',
        },
        email: {
          required: 'Lütfen email adresinizi girin.',
        },
        type: {
          required: 'Lütfen şirket türünü seçin.',
        }       
      },
      submitHandler: function(form) {
        var requestMessage;
        $.ajax({
          type: "POST",
          url: "send/basvurumail.php",
          data: $(form).serialize(),
          success: function ($data) {
            if ($data == 1){
              requestMessage = 'Başvurunuzu aldık, en kısa sürede dönüş yapacağız.'
            } else {
              requestMessage = 'Bir sorun oluştu. Lütfen daha sonra tekrar deneyiniz.'
            }
            $('.js-apply-form-status').text(requestMessage);
          },
          error: function () {
            requestMessage = 'Bir sorun oluştu. Lütfen daha sonra tekrar deneyiniz.'
            $('.js-apply-form-status').text(requestMessage);
          }
        });
      }      
    });
  },

  faqToggle: function() {
    $('.js-faq-toggle').on('click', function() {
      $(this).parent().toggleClass('active');
    });
  }
}

$(function() {
  paytrek.init();
});
